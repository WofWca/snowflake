package websocketconn

import "fmt"

type DialOptions struct {
	// Whether the proxy should perform subprotocol negotiation
	// with the WebSocket server (hopefully a Snowflake server).
	//
	// This is an additional hardening measure, to ensure that the proxy
	// can only connect to Snowflake servers and not just any WebSocket server.
	//
	// When this is set to `true`, if the server doesn't pick the Snowflake
	// subprotocol (see the `theTrueSubprotocol` const),
	// the WebSocket connection will fail.
	// See https://www.rfc-editor.org/rfc/rfc6455.html#section-1.3
	RequireSubprotocolNegotiation bool
}

const theTrueSubprotocol = "the-true-protocol-snowflake.torproject.org"

var proxySubprotocols []string = []string{
	// Have two other subprotocols as "decoys", in case e.g.
	// it's actually a non-Snowflake server that just mindlessly picks
	// the first / last subprotocol suggested by the client.
	"dont-pick-me-1",
	theTrueSubprotocol,
	"dont-pick-me-2",
}

type wsConn interface {
	Subprotocol() string
}

func verifyCorrectSubprotocol(ws wsConn) error {
	selectedSubprotocol := ws.Subprotocol()
	if selectedSubprotocol == theTrueSubprotocol {
		return nil
	}
	return fmt.Errorf(
		"the server picked the wrong protocol \"%v\", expected %v",
		selectedSubprotocol,
		theTrueSubprotocol,
	)
}
