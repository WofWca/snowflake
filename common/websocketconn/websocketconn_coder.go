//go:build js || coder_websocket_lib

package websocketconn

import (
	"context"
	"net"
	"net/http"

	websocket "github.com/coder/websocket"
)

type wsConnWrapper struct {
	*websocket.Conn
	ctx           *context.Context
	cancelContext *context.CancelFunc
}

type wsNetConnWrapper struct {
	net.Conn
	cancelContext *context.CancelFunc
}

func (connWrapper *wsNetConnWrapper) Close() error {
	err := connWrapper.Conn.Close()
	(*connWrapper.cancelContext)()

	return err
}

func Dial(
	url string,
	options DialOptions,
) (*wsConnWrapper, *http.Response, error) {
	ctx, cancel := context.WithCancel(context.Background())

	var options2 *websocket.DialOptions = nil
	if options.RequireSubprotocolNegotiation {
		options2 = &websocket.DialOptions{
			Subprotocols: proxySubprotocols,
		}
	}

	c, httpResponse, err := websocket.Dial(ctx, url, options2)
	if err != nil {
		cancel()
		return nil, httpResponse, err
	}

	if options.RequireSubprotocolNegotiation {
		if err := verifyCorrectSubprotocol(c); err != nil {
			cancel()
			return nil, nil, err
		}
	}

	return &wsConnWrapper{
		Conn:          c,
		ctx:           &ctx,
		cancelContext: &cancel,
	}, httpResponse, nil
}

// With the `coder_websocket_lib` build tag, this is a no-op
// (it simply returns `nil`)
func NewServerUpgrader() any {
	return nil
}

func Upgrade(
	upgrader any,
	w http.ResponseWriter,
	r *http.Request,
) (*wsNetConnWrapper, error) {
	c, err := websocket.Accept(w, r, &websocket.AcceptOptions{
		Subprotocols: []string{theTrueSubprotocol},
	})
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(r.Context())

	netConn := websocket.NetConn(ctx, c, websocket.MessageBinary)
	return &wsNetConnWrapper{
		Conn:          netConn,
		cancelContext: &cancel,
	}, nil
}

// Create a new Conn.
func New(ws *wsConnWrapper) net.Conn {
	// TODO ensure all the erros (EOF) stuff behave as we want them to,
	// see `closeErrorToEOF` in Gorilla version.
	// This appears to only affect server logging though.

	netConn := websocket.NetConn(*ws.ctx, ws.Conn, websocket.MessageBinary)
	return &wsNetConnWrapper{
		Conn:          netConn,
		cancelContext: ws.cancelContext,
	}
}
