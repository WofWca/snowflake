//go:build js

// Provides functions that work in non-JavaScript builds,
// but panic in JavaScript builds.
//
// This is to work around the fact that the js,wasm version of the Pion library
// does not implement these methods.
// This would cause the JS build of Snowflake to fail
// if we were to use those methods unconditionally.
package jsnotjs

import "log"

const notSupportedErrorMsg = "This is not supported in JS"

func SetNetIfNotJs(args ...any) {
	log.Fatal(notSupportedErrorMsg)
}

func SetEphemeralUDPPortRangeIfNotJs(args ...any) any {
	log.Fatal(notSupportedErrorMsg)
	return nil
}

func SetNAT1To1IPsIfNotJs(args ...any) {
	log.Fatal(notSupportedErrorMsg)
}

func SetICEMulticastDNSModeIfNotJs(args ...any) {
	log.Fatal(notSupportedErrorMsg)
}

func SetDTLSInsecureSkipHelloVerifyIfNotJs(args ...any) {
	log.Fatal(notSupportedErrorMsg)
}

func SetIPFilterIfNotJs(args ...any) {
	log.Fatal(notSupportedErrorMsg)
}

func SetIncludeLoopbackCandidateIfNotJs(arg ...any) {
	log.Fatal(notSupportedErrorMsg)
}
