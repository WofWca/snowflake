//go:build !js

// Provides functions that work in non-JavaScript builds,
// but panic in JavaScript builds.
//
// This is to work around the fact that the js,wasm version of the Pion library
// does not implement these methods.
// This would cause the JS build of Snowflake to fail
// if we were to use those methods unconditionally.
package jsnotjs

import (
	"net"

	"github.com/pion/ice/v4"
	"github.com/pion/transport/v3"
	"github.com/pion/webrtc/v4"
)

func SetNetIfNotJs(s *webrtc.SettingEngine, net transport.Net) {
	s.SetNet(net)
}

func SetEphemeralUDPPortRangeIfNotJs(s *webrtc.SettingEngine, portMin uint16, portMax uint16) error {
	return s.SetEphemeralUDPPortRange(portMin, portMax)
}

func SetNAT1To1IPsIfNotJs(s *webrtc.SettingEngine, ips []string, candidateType webrtc.ICECandidateType) {
	s.SetNAT1To1IPs(ips, candidateType)
}

func SetICEMulticastDNSModeIfNotJs(s *webrtc.SettingEngine, multicastDNSMode ice.MulticastDNSMode) {
	s.SetICEMulticastDNSMode(multicastDNSMode)
}

func SetDTLSInsecureSkipHelloVerifyIfNotJs(s *webrtc.SettingEngine, skip bool) {
	s.SetDTLSInsecureSkipHelloVerify(true)
}

func SetIPFilterIfNotJs(s *webrtc.SettingEngine, filter func(net.IP) (keep bool)) {
	s.SetIPFilter(filter)
}

func SetIncludeLoopbackCandidateIfNotJs(s *webrtc.SettingEngine, include bool) {
	s.SetIncludeLoopbackCandidate(include)
}
