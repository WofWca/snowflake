chrome.offscreen.createDocument({
  url: 'index.html',
  reasons: [chrome.offscreen.Reason.WEB_RTC],
  justification: 'Use WebRTC.',
});
