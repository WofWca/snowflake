const go = new Go();
go.argv = [
  "",
  "-verbose",
  // Same as the in the extension version.
  // However, the intervali in the extension version is not fixed.
  // https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/blob/c920540d7b99f9c848c3229ec274d1079620a26f/config.js#L23-26
  "-poll-interval",
  "60s",
  // Same as the in the extension version.
  // However, the extension version sets capacity to 2
  // if the NAT is unrestricted.
  // https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/blob/68d41fdd70b59520db07dbdc9dd9b6d4eaf99e79/config.js#L43
  "-capacity",
  "1",
];
WebAssembly.instantiateStreaming(fetch("./proxy.wasm"), go.importObject).then(
  (result) => {
    go.run(result.instance);
  }
);
